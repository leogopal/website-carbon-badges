const $q = (selector) => document.getElementById(selector);
const url = encodeURIComponent(window.location.href)

const newRequest = function (render = true) {
    // Run the API request because there is no cached result available
    fetch('https://api.websitecarbon.com/b?url=' + url)
        .then(function (r) {
            if (!r.ok) {
                throw Error(r);
            }
            return r.json();
        })

        .then(function (r) {
            if (render) {
                renderResult(r)
            }

            // Save the result into localStorage with a timestamp
            r.t = new Date().getTime()
            localStorage.setItem('wcb_'+url, JSON.stringify(r))
        })

        // Handle error responses
        .catch(function (e) {
            $q('wcb_g').innerHTML = 'No Result';
            console.log(e);
            localStorage.removeItem('wcb_'+url)
        })
}

const renderResult = function (r) {
    $q('wcb_g').innerHTML = r.c + 'g of CO<sub>2</sub>/view'
    $q('wcb_2').insertAdjacentHTML('beforeEnd', 'Cleaner than ' + r.p + '% of pages tested')
}

// Get the CSS and add it to the DOM. The placeholder will be filled by gulp build
const css = '{{css}}';
const badge = $q('wcb');

if (('fetch' in window)) { // If the fetch API is not available, don't do anything.
    badge.insertAdjacentHTML('beforeEnd',css)

    // Add the badge markup
    badge.insertAdjacentHTML('beforeEnd', '<div id="wcb_p"><p id="wcb_g">Measuring CO<sub>2</sub>&hellip;</p><a target="_blank" rel="noopener" href="https://websitecarbon.com">Website Carbon</a></div><p id="wcb_2"></p>');

    // Get result if it's saved
    let cachedResponse = localStorage.getItem('wcb_' + url)
    const t = new Date().getTime()

    // If there is a cached response, use it
    if (cachedResponse) {
        const r = JSON.parse(cachedResponse)
        renderResult(r)

        // If time since response was cached is over a day, then make a new request and update the cached result in the background
        if ((t - r.t) > (86400000)) {
            newRequest(false)
        }

    // If no cached response, then fetch from API
    } else {
        newRequest()
    }
}
